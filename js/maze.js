// Maze game module
function Maze() {
}

(function($) {
"use strict"

var M = Maze; // a shortcut to this module

var canv = $("#c");
var gl = twgl.getWebGLContext(document.getElementById("c"));
var programInfo = twgl.createProgramInfo(gl, ["vs", "fs"]);

var arrays = {
	position: [-1, -1, 0, 1, -1, 0, -1, 1, 0, -1, 1, 0, 1, -1, 0, 1, 1, 0],
};

var cell = null; // array of cells representing the map internally
var mincellside = 60; // minimum cell size so that a finger can fit into it
var cellside = 60; // side of one cell in pixels
var hwratio = 0.07; // halfwall ratio
var halfwall = 0; // half of wall's width in pixels
var mazemap = { tex: null, res: [20, 10] }; // map of the maze
var playercell = 0; // index of the cell the player is currently in right now
var prevcell = 0; // index of the previous playercell
var entrycell = 0; // index of the entry cell
var exitcell = 0; // index of exit cell
var difficulty = 0; // don't limit difficulty
var d2side = [0, 7, 10, 12]; // difficulty level to square maze's side
var playersprite = $("#playersprite");
var exitsprite = $("#exitsprite");

var bufferInfo = twgl.createBufferInfoFromArrays(gl, arrays);

M.clearmaze = function() {
	var ncells = mazemap.res[0] * mazemap.res[1];
	for(var i = 0; i < ncells; ++i) {
		cell[i] &= ~240;
	}
	prevcell = entrycell;
	playercell = entrycell;
	cell[playercell] |= 224;
	requestAnimationFrame(render);
};

M.showhowto = function() {
	$("#howtoplay").show();
};

$("#howtoplay").click(function() { $(this).hide(); });

M.showyouwon = function() {
	$("#youwon").show();
};

$("#youwon").click(function() {
	$(this).hide();
	M.nextmaze();
});

M.nextmaze = function() {
	mkmap();
};

function mkmaze(cell, res) {
	var ncells = res[0] * res[1];
	var cur = Math.floor(Math.random() * (ncells + 1)); // current cell ind
	var stack = [];
	stack[0] = cur; // add first cell to the stack
	var visitorder = [1, 2, 4, 8]; // order in which to visit neighbors,
		// where 1 is top, 2 is right, 4 is bottom, 8 is left

	while(stack.length) {
		// try to visit a nearby cell. We randomly an order in which
		// we try to visit the cells by shuffling the visitorder array:
		visitorder.sort(function() { return 0.5 - Math.random() });
		var visited = 0; // have we visited any cell yet?
		for(var i = 0; i < 4; ++i) {
			// try to visit selected cell
			switch(visitorder[i]) {
			case 1:
				// visiting the cell above if possible
				if(cur + res[0] >= ncells ||
						cell[cur + res[0]]) {
					// can't visit cell above
					break;
				}
				cell[cur] |= 1; // remove top wall
				cur += res[0]; // make top cell the current one
				cell[cur] |= 4; // remove bottom wall
				stack.push(cur); // push current cell
				visited = 1;
				break;
			case 2:
				// visiting the cell to the right
				if(cur % res[0] == res[0] - 1
							|| cell[cur + 1]) {
					break;
				}
				cell[cur] |= 2;
				++cur;
				cell[cur] |= 8;
				stack.push(cur);
				visited = 1;
				break;
			case 4:
				// visiting bottom cell
				if(cur - res[0] < 0 || cell[cur - res[0]]) {
					// we are in the bottom, no way down
					break;
				}
				cell[cur] |= 4;
				cur -= res[0];
				cell[cur] |= 1;
				stack.push(cur);
				visited = 1;
				break;
			case 8:
				// visiting left cell
				if(!(cur % res[0]) || cell[cur - 1]) {
					break;
				}
				cell[cur] |= 8;
				--cur;
				cell[cur] |= 2;
				stack.push(cur);
				visited = 1;
				break;
			}
			if(visited) {
				break;
			}
		}
		if(!visited) {
			// we could not visit any unvisited cell from here,
			// so we need to go to previous cell and start over:
			cur = stack.pop();
		}
	}
}

// set cellside and automatically adjust halfwall:
function setcside(npixels, ncells) {
	cellside = ncells? Math.floor(npixels / (2 * 0.1 + ncells)): npixels;
	halfwall = Math.floor(cellside * hwratio);
}

// make map and init the game
function mkmap() {
	setcside(mincellside);
	// extract difficulty level from the url:
	var arr = document.URL.match(/d=([0-9]+)/)
	difficulty = arr? arr[1]: 0; // difficulty level. 0=unlimited 1=easy
	var side = d2side[difficulty]; // target square's side for the maze
	var maxsz = side * side; // maximum number of cells allowed in the maze
	// adjust size of the map:
	var wrapwidth = $("#cwrap").width(); // wrapping div's width
	mazemap.res[0] = Math.floor(wrapwidth / cellside); // map's width, cells
	if(mazemap.res[0] * cellside + 2 * halfwall > wrapwidth) {
		--mazemap.res[0];
	}
	// get window's height:
	var availheight = Math.floor($(window).height() - canv.offset().top);
	mazemap.res[1] = Math.floor(availheight / cellside);
	if(mazemap.res[1] * cellside + 2 * halfwall > availheight) {
		--mazemap.res[1];
	}
	var fieldsize = mazemap.res[0] * mazemap.res[1];

	// apply diffiiculty limit to the field size:
	if(maxsz && fieldsize > maxsz) {
		if(mazemap.res[0] > side) {
			if(mazemap.res[1] > side) {
				// we have plenty of space to make square maze:
				if(mazemap.res[0] > mazemap.res[1]) {
					// landscape, so use height as limiting
					// factor:
					setcside(availheight, side);
				} else {
					setcside(wrapwidth, side);
				}
				mazemap.res[0] = mazemap.res[1] = side;
			} else {
				// we are in landscape mode, limit width:
				mazemap.res[0] =
					Math.floor(maxsz / mazemap.res[1]);
			}
		} else {
			// we are in portrait mode, limit height:
			mazemap.res[1] = Math.floor(maxsz / mazemap.res[0]);
		}
	}

	// we must recalculate fieldsize after above adjustments
	fieldsize = mazemap.res[0] * mazemap.res[1];

	var canvwidth = mazemap.res[0] * cellside + 2 * halfwall;
	var canvheight = mazemap.res[1] * cellside + 2 * halfwall;
	canv.css({width: canvwidth, height: canvheight});

	cell = new Uint8Array(fieldsize);
	// following the opengl coordinate system, our first cell will be
	// in the bottom left corner.
	// generate the maze now:
	mkmaze(cell, mazemap.res);
	entrycell = Math.floor(mazemap.res[1] * 0.5) * mazemap.res[0];
	exitcell = entrycell - 1;
	cell[entrycell] |= 232; // mark entry cell as such
	cell[exitcell] |= 2; // exit cell
	playercell = prevcell = entrycell;
//	moveplayersprite();
	exitsprite.css({width: cellside, height: cellside,
		top: canv.offset().top + halfwall + cellside * (mazemap.res[1] -
			Math.floor(exitcell / mazemap.res[0]) - 1),
		left: canv.offset().left + halfwall +
				cellside * (exitcell % mazemap.res[0])});

// 32 top->right
// 64 right->bottom
// 96 bottom->left
// 128 left->top
// 160 top->bottom
// 192 left->right
// 224 player's position

	requestAnimationFrame(render);
}

// move player's sprite to where the player currently is on the map
function moveplayersprite() {
	playersprite.css({width: cellside, height: cellside,
		top: canv.offset().top + halfwall + cellside * (mazemap.res[1] -
			Math.floor(playercell / mazemap.res[0]) - 1),
		left: canv.offset().left + halfwall +
				cellside * (playercell % mazemap.res[0])});
}

// creates path on player's position, connecting prevcell and tocell:
function trypath(tocell) {
	if(tocell == playercell) return 0;
	if(tocell == prevcell) { // move back
		cell[playercell] &= ~224;
		return 1;
	}
	var fromdir = prevcell - playercell;
	var todir = tocell - playercell;
	if(fromdir == mazemap.res[0]) { // moving from top
		if(todir == 1) { // top->right:
			if(!(cell[playercell] & 2)) return 0; // wall blocking
			cell[playercell] &= ~240;
			cell[playercell] |= 32;
			return 1;
		} else if(todir == -mazemap.res[0]) { // top->down:
			if(!(cell[playercell] & 4)) return 0;
			cell[playercell] &= ~240;
			cell[playercell] |= 160;
			return 1;
		} else if(todir == -1) { // top->left:
			if(!(cell[playercell] & 8)) return 0;
			cell[playercell] &= ~240;
			cell[playercell] |= 144;
			return 1;
		}
	} else if(fromdir == 1) { // moving from right
		if(todir == mazemap.res[0]) { // right->top
			if(!(cell[playercell] & 1)) return 0;
			cell[playercell] &= ~240;
			cell[playercell] |= 48;
			return 1;
		} else if(todir == -mazemap.res[0]) { // right->bottom
			if(!(cell[playercell] & 4)) return 0;
			cell[playercell] &= ~240;
			cell[playercell] |= 64;
			return 1;
		} else if(todir == -1) { // right->left
			if(!(cell[playercell] & 8)) return 0;
			cell[playercell] &= ~240;
			cell[playercell] |= 208;
			return 1;
		}
	} else if(fromdir == -mazemap.res[0]) { // moving from bottom
		if(todir == mazemap.res[0]) { // bottom->top
			if(!(cell[playercell] & 1)) return 0;
			cell[playercell] &= ~240;
			cell[playercell] |= 176;
			return 1;
		} else if(todir == 1) { // bottom->right
			if(!(cell[playercell] & 2)) return 0;
			cell[playercell] &= ~240;
			cell[playercell] |= 80;
			return 1;
		} else if(todir == -1) { // bottom->left
			if(!(cell[playercell] & 8)) return 0;
			cell[playercell] &= ~240;
			cell[playercell] |= 96;
			return 1;
		}
	} else if(fromdir == -1 || !fromdir) { // moving from left
		if(todir == mazemap.res[0]) { // left->top
			if(!(cell[playercell] & 1)) return 0; // wall blocking
			cell[playercell] &= ~240;
			cell[playercell] |= 128;
			return 1;
		} else if(todir == 1) { // left->right
			if(!(cell[playercell] & 2)) return 0;
			cell[playercell] &= ~240;
			cell[playercell] |= 192;
			return 1;
		} else if(todir == -mazemap.res[0]) { // moving from left down:
			if(!(cell[playercell] & 4)) return 0; // wall blocking
			cell[playercell] &= ~240;
			cell[playercell] |= 112;
			return 1;
		}
	}
	return 0;
}

function ondrag(e) {
	// mouse position relative to corner of the 0th cell:
	var mousepos = [e.px_current_x - canv.offset().left - halfwall,
		e.px_current_y - canv.offset().top - halfwall];
	if(mousepos[0] < 0 || mousepos[1] < 0 ||
		mousepos[0] > mazemap.res[0] * cellside ||
		mousepos[1] > mazemap.res[1] * cellside) {
		return;
	}
	// invert y so that our cell 0 is in the bottom left corner:
	mousepos[1] = mazemap.res[1] * cellside - mousepos[1];
	// determine the index of the cell the mouse is in:
	var mousecell = Math.floor(mousepos[1] / cellside) * mazemap.res[0] +
		Math.floor(mousepos[0] / cellside);
	if(playercell == exitcell) return;
	if(trypath(mousecell)) {
		if(prevcell == mousecell) { // going back
			switch(cell[prevcell] & 240) {
			case 32:
			case 144:
			case 160: prevcell = prevcell + mazemap.res[0]; break;
			case 64:
			case 48:
			case 208: prevcell = prevcell + 1; break;
			case 96:
			case 176:
			case 80: prevcell = prevcell - mazemap.res[0]; break;
			case 128:
			case 192:
			case 112: prevcell = prevcell - 1; break;
			}
			if(mousecell == entrycell) {
				prevcell = entrycell;
			}
		} else {
			prevcell = playercell;
		}
		playercell = mousecell;
		cell[playercell] |= 224;
		requestAnimationFrame(render);
	}
}

// what to do if the player taps inside the canvas:
canv.bind("utap", ondrag);
canv.bind("udragstart", ondrag).bind("udragmove", ondrag);

mkmap();

function render(time) {
	moveplayersprite();
	mazemap.tex = twgl.createTexture(gl, {
			width: mazemap.res[0],
			height: mazemap.res[1],
			mag: gl.NEAREST,
			min: gl.NEAREST,
			format: gl.LUMINANCE,
			src: cell
		});

	gl.enable(gl.BLEND);
	gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);

	twgl.resizeCanvasToDisplaySize(gl.canvas);
	gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);

	var uniforms = {
		time: time * 0.001,
		resolution: [gl.canvas.width, gl.canvas.height],
		mazemap: mazemap.tex,
		mapres: mazemap.res,
		cellparam: [cellside, halfwall]
	};

	gl.useProgram(programInfo.program);
	twgl.setBuffersAndAttributes(gl, programInfo, bufferInfo);
	twgl.setUniforms(programInfo, uniforms);
	twgl.drawBufferInfo(gl, gl.TRIANGLES, bufferInfo);

	if(playercell == exitcell) {
		M.showyouwon();
	}

	//requestAnimationFrame(render);
}

})(jQuery);
